import sys, os
sys.path.append('/var/local/intoinfinity')
sys.stdout = sys.stderr

import atexit
import threading

import cherrypy

from infinity import Root

if cherrypy.engine.state == 0:
    cherrypy.engine.start(blocking=False)
    atexit.register(cherrypy.engine.stop)

application = cherrypy.Application(Root(), '/')
application.merge(config="/var/local/intoinfinity/settings.conf")


