var eyes = [], pp = true;

function random_eye(){
    jQuery.ajax({
	    type: "GET",
	    url:"/json/random",
	    data: "document_type=Eye",
	    dataType: "json",
	    processData: true,
	    success: function(data){
		eyes.push(data);
		play();
	    },
	    error: function(){random_eye()}
	});
}


function update(c, one,two,three){
    if (c >= 3){
	jQuery("#one > img").attr("src",one);
	jQuery("#two > img").attr("src",two);
	jQuery("#three > img").attr("src",three);
	setTimeout("next()",3500);
    }
}

function play(){
    if (pp){
        if (eyes.length == 3){
	    var one = "/documents/"+eyes[2]["gallery_path"], 
		two = "/documents/"+eyes[1]["medium_path"], 
		three = "/documents/"+eyes[0]["small_path"],
		c = 0;

	    jQuery.ajax({
		    type: "GET",
		    url: one,
		    dataType: "html",
		    success: function() {c += 1;update(c, one,two,three);}
		});


	    jQuery.ajax({
		    type: "GET",
		    url: two,
		    dataType: "html",
		    success: function() {c += 1;update(c, one,two,three);}
		});

	    jQuery.ajax({
		    type: "GET",
		    url: three,
		    dataType: "html",
		    success: function() {c += 1;update(c, one,two,three);}
		});


        } else {
	    random_eye();
        }
    }
}

function next(){
    eyes.shift();
    random_eye();
}

jQuery(document).ready(function(){
	random_eye();
	jQuery("#play").mouseover(function(e){jQuery(this).attr("src","/media/play_white.png");});
	jQuery("#play").mouseout(function(e){if(pp !=true){jQuery(this).attr("src","/media/play_black.png");}});
	jQuery("#pause").mouseover(function(e){jQuery(this).attr("src","/media/pause_white.png");});
	jQuery("#pause").mouseout(function(e){if(pp){jQuery(this).attr("src","/media/pause_black.png");}});
	jQuery("#pause").click(function(e){pp=false;jQuery(this).attr("src","/media/pause_white.png");jQuery("#play").attr("src","/media/play_black.png");});
	jQuery("#play").click(function(e){if(pp==false){next();pp=true;jQuery(this).attr("src","/media/play_white.png");jQuery("#pause").attr("src","/media/pause_black.png");}});

    });
